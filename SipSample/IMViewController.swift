//
//  IMViewController.m
//  SIPSample
//

//  Copyright (c) 2013 PortSIP Solutions, Inc. All rights reserved.
//
import Foundation
class IMViewController:UIViewController,UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource{
    
    var portSIPSDK:PortSIPSDK!;
    
    @IBOutlet var textContact:UITextField!;
    @IBOutlet var textMessage:UITextField!;
    @IBOutlet var tableView:UITableView!;
    var contacts:Array<Contact>!;
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        textContact.delegate = self;
        textMessage.delegate = self;
        // Do any additional setup after loading the view.
        
        contacts = [];
        self.tableView.autoresizingMask = UIViewAutoresizing.flexibleHeight;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardWillShow(_ noti:NSNotification)
    {
        let height:CGFloat = 216.0;
        var frame = self.view.frame;
        frame.size = CGSize(width:frame.size.width,height: frame.size.height - height);
        UIView.beginAnimations("Curl", context:nil);
        UIView.setAnimationDuration(0.30);
        UIView.setAnimationDelegate(self);
        self.view.frame = frame;
        UIView.commitAnimations();
    }
    
    
    func textFieldShouldReturn(_ textField:UITextField)->Bool
    {
        // When the user presses return, take focus away from the text field so that the keyboard is dismissed.
        let animationDuration:TimeInterval = 0.30;
        UIView.beginAnimations("ResizeForKeyboard",context:nil);
        UIView.setAnimationDuration(animationDuration);
        let rect = CGRect(origin:CGPoint(x:0.0, y:0.0), size:CGSize(width:self.view.frame.size.width, height:self.view.frame.size.height));
        self.view.frame = rect;
        UIView.commitAnimations();
        textField.resignFirstResponder();
        return true;
    }
    
    
    func textFieldDidBeginEditing(_ textField:UITextField)
    {
        let frame = textField.frame;
        let offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0);
        let animationDuration:TimeInterval = 0.30;
        UIView.beginAnimations("ResizeForKeyBoard", context:nil);
        UIView.setAnimationDuration(animationDuration);
        
        let width = self.view.frame.size.width;
        let height = self.view.frame.size.height;
        
        if(offset > 0)
        {
            let rect = CGRect(origin:CGPoint(x:0.0, y:-offset),size:CGSize(width:width,height:height));
            self.view.frame = rect;
        }
        UIView.commitAnimations();
    }
    
    @IBAction func onSubscribeClick(_ sender:AnyObject)
    {
//        let subscribeID = portSIPSDK.presenceSubscribeContact(textContact.text,subject:"hello");
        let subscribeID = portSIPSDK.presenceSubscribe(textContact.text,subject:"hello");
        
        let contact = Contact.init(subscribeid: Int(subscribeID),andSipURL:textContact.text!);
        
        contacts.append(contact)
        tableView.reloadData();
    }
    
    @IBAction func onOnlineClick(_ sender:AnyObject)
    {
        for contact in contacts
        {
//            portSIPSDK.presenceOnline(contact.subscribeID,statusText:"I'm here");
            portSIPSDK.setPresenceStatus(contact.subscribeID, statusText: "I'm here");
        }
    }
    
    
    @IBAction func onOfflineClick(_ sender:AnyObject)
    {
        for contact in contacts
        {
            
//            portSIPSDK.presenceOffline(contact.subscribeID);
            portSIPSDK.setPresenceStatus(contact.subscribeID, statusText: "offline");
            
        }
    }
    
    @IBAction func onSendMessageClick(_ sender:AnyObject)
    {
        let message = textMessage.text?.data(using: String.Encoding.utf8) ;
//        dataUsingEncoding:NSUTF8StringEncoding;
        
        let messageID = portSIPSDK.sendOut(ofDialogMessage: textContact.text,mimeType:"text",subMimeType:"plain", isSMS: false, message:message, messageLength:Int32((message?.count)!));
        
        NSLog("send Message %d",messageID);
    }
    
    //Instant Message/Presence Event
    func onSendMessageSuccess(_ messageId:Int)
    {
        NSLog("%zd message send success",messageId);
        return ;
    }
    
    func onSendMessageFailure(_ messageId:Int, reason:String, code:Int)
    {
        NSLog("%zd message send failure",messageId);
        return ;
    }
    
    func alertView(_ alertView:UIAlertView,clickedButtonAtIndex buttonIndex:Int)
    {
        let subjectId = alertView.tag;
        if(buttonIndex == 0){//Reject Subscribe
            portSIPSDK.presenceRejectSubscribe(subjectId);
        }
        else if (buttonIndex == 1){//Accept Subscribe
            for contact in contacts
            {
                if(contact.subscribeID == subjectId){
                    portSIPSDK.presenceAcceptSubscribe(subjectId);
//                    portSIPSDK.presenceOnline(subjectId,statusText:"Available");
                    portSIPSDK.setPresenceStatus(subjectId, statusText: "Available");
                    
//                    portSIPSDK.presenceSubscribeContact(contact.sipURL, subject:"Hello");
                    portSIPSDK.presenceSubscribe(contact.sipURL, subject: "Hello");
                }
            }
        }
    }
    
    func onPresenceRecvSubscribe(_ subscribeId:Int,
                                 fromDisplayName:String,
                                 from:String,
                                 subject:String)->Int
    {
        for contact in contacts
        {
            
            if(contact.sipURL==from)
            {//has exist this contact
                //update subscribedId
                contact.subscribeID = subscribeId;
                
                //Accept subscribe.
                portSIPSDK.presenceAcceptSubscribe(subscribeId);
//                portSIPSDK.presenceOnline(subscribeId, statusText:"Available");
                portSIPSDK.setPresenceStatus(subscribeId, statusText: "Available");
                return 0;
            }
        }
        
        let contact = Contact.init(subscribeid: subscribeId,andSipURL: from);
        
        contacts.append(contact);
        tableView.reloadData();
        
        let alert = UIAlertView();
        alert.title = "Recv Subscribe";
        alert.message = String(format:"Recv Subscribe <%s>%s: %s",fromDisplayName,from,subject);
        alert.delegate = self;
        //            cancelButtonTitle: "Reject"
        //            otherButtonTitles:"Accept", nil;
        alert.tag = subscribeId;
        alert.addButton(withTitle: "Reject");
        alert.addButton(withTitle: "Accept");
        alert.show();
        return 0;
    }
    
    func onPresenceOnline(_ fromDisplayName:String,
                          from:String,
                          stateText:String)
    {
        for contact in contacts
        {
            
            if(contact.sipURL == from)
            {
                contact.basicState = "open";
                contact.note = stateText;
                tableView.reloadData();
                break;
            }
            
        }
    }
    
    func onPresenceOffline(_ fromDisplayName:String, from:String)
    {
        for contact in contacts
        {
            
            if(contact.sipURL == from)
            {
                contact.basicState = "close";
                tableView.reloadData();
                break;
            }
        }
    }
    
    //        #pragma mark - Table view data source
    
    func numberOfSections(in tableView:UITableView )->Int
    {
        return 1;
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int)->Int
    {
        if(0 == section){
            return contacts.count;
        }
        
        return 0;
    }
    
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath)->UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCellIdentifier") as! ContactCell;
        
        //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        
        if(contacts.count > indexPath.row){
            let contact = contacts[indexPath.row];
  
                cell.urlLabel.text = contact.sipURL;
                cell.noteLabel.text = contact.note;
                if(contact.basicState == "open")
                {
                    cell.onlineImageView.image = UIImage.init(contentsOfFile: "online.png");
                }
                else
                {
                    cell.onlineImageView.image = UIImage.init(contentsOfFile: "offline.png");
                }

        }
        
        return cell;
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView:UITableView, commit editingStyle:UITableViewCellEditingStyle, forRowAt indexPath:IndexPath)
    {
        
        if (editingStyle == UITableViewCellEditingStyle.delete) {
//            let contact = contacts[indexPath.row];
//            if (contact) {
//                mPortSIPSDK presenceUnsubscribeContact :contact.subscribeID;
//            }
            contacts.remove(at: indexPath.row);
            tableView.deleteRows(at: [indexPath], with:UITableViewRowAnimation.fade);
        }
    }
}

//
//  NetParamsController.swift
//  SIPSample
//
//  Created by portsip on 16/6/30.
//  Copyright © 2016 PortSIP. All rights reserved.
//
import UIKit
protocol NetParamsControllerDelegate{
    func didSelectValue(_ key:String,value:Int)
    
//    func didEndSelectValue(key:String,value:Int)
}

class NetParamsController:UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet var tbView:UITableView!;
    var data:Array<String>! = [];
    var labletitle:String!;
    var delegate:AnyObject!;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationController!.setNavigationBarHidden(false,animated: false);
        //self.navigationController!.title = labletitle;
        tbView.dataSource = self;
        tbView.delegate = self;
        
        print(data);
        
        
    }
    
    @IBAction func backButtonClicked(_ sender:AnyObject){
        self.navigationController!.popToRootViewController(animated: true);
    }
    
    
    func  tableView(_ tableView:UITableView ,numberOfRowsInSection section:Int)->Int{
        if(self.data != nil)
        {
            return self.data.count;
        }
        return 0;
    }
    
    func tableView(_ tableView:UITableView ,cellForRowAt indexPath:IndexPath)->UITableViewCell{
        let cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier:self.data[(indexPath as NSIndexPath).row]);
        cell.textLabel!.text = self.data[(indexPath as NSIndexPath).row];
        return cell;
    }
    
    
    func itemCellHeight(_ indexPath:IndexPath)->Float
    {
        return 44.0;
    }
    
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        self.navigationController!.popToRootViewController(animated: true);
        let tempdelegate = delegate as! NetParamsControllerDelegate
        tempdelegate.didSelectValue(self.labletitle,value: (indexPath as NSIndexPath).row);
    }
}

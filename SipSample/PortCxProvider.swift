//
//  PortCxProvider.swift
//  SipSample
//
//  Created by portsip on 17/2/22.
//  Copyright © 2017 portsip. All rights reserved.
//

import UIKit
import CallKit

@available(iOS 10.0, *)
class PortCxProvider: NSObject ,CXProviderDelegate{
    
    var cxprovider:CXProvider!
    var callManager:CallManager!
    var callController:CXCallController!
    private static var instance:PortCxProvider = PortCxProvider()
    
    class var shareInstance: PortCxProvider {
        return PortCxProvider.instance
    }
    
    override init(){
        super.init()
        configurationCallProvider()
    }
    
    func configurationCallProvider() {
        let infoDic = Bundle.main.infoDictionary!
        let localizedName = infoDic["CFBundleName"] as! String
        
        let providerConfiguration = CXProviderConfiguration(localizedName: localizedName)
        providerConfiguration.supportsVideo = true;
        providerConfiguration.maximumCallsPerCallGroup = 1
        providerConfiguration.supportedHandleTypes = [.phoneNumber]
        if let iconMaskImage = UIImage(named: "IconMask") {
            providerConfiguration.iconTemplateImageData = UIImagePNGRepresentation(iconMaskImage)
        }
        
        cxprovider = CXProvider.init(configuration: providerConfiguration)
        
        cxprovider.setDelegate(self, queue: DispatchQueue.main)
        
        callController = CXCallController()
    }
    
    func reportOutgoingCall(callUUID:UUID , startDate:Date) -> (UUID) {
        cxprovider.reportOutgoingCall(with: callUUID, connectedAt: startDate)
        return callUUID
    }
    

    
//    #pragma mark - CXProviderDelegate
    
    func providerDidReset(_ provider: CXProvider) {
        callManager.stopAudio()
        print("Provider did reset")
        
        callManager.clear();
    }

    func provider(_ provider: CXProvider, perform action: CXPlayDTMFCallAction) {
        print(" CXPlayDTMFCallAction \(action.callUUID) \(action.digits)")
        
        var dtmf:Int32 = 0
        switch action.digits {
        case "0":
            dtmf = 0;
            break;
        case "1":
            dtmf = 1;
            break;
        case "2":
            dtmf = 2;
            break;
        case "3":
            dtmf = 3;
            break;
        case "4":
            dtmf = 4;
            break;
        case "5":
            dtmf = 5;
            break;
        case "6":
            dtmf = 6;
            break;
        case "7":
            dtmf = 7;
            break;
        case "8":
            dtmf = 8;
            break;
        case "9":
            dtmf = 9;
            break;
        case "*":
            dtmf = 10;
            break;
        case "#":
            dtmf = 11;
            break;
        default:
            return;
        }
        callManager.sendDTMF(uuid: action.callUUID, dtmf: dtmf)
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetGroupCallAction) {
        guard (callManager.findCallByUUID(uuid: action.callUUID) != nil) else {
            action.fail()
            return
        }
        
        if action.callUUIDToGroupWith != nil {
            callManager .joinToConference(uuid: action.callUUID)
            action.fulfill()
        } else {
            callManager.removeFromConference(uuid: action.callUUID)
            action.fulfill()
        }
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("performAnswerCallAction uuid = \(action.callUUID)")
        
        if let result = callManager.findCallByUUID(uuid: action.callUUID) {
            if callManager.answerCallWithUUID(uuid: action.callUUID, isVideo: result.session.videoState) {
                action.fulfill()
                return
            }
        }
        action.fail()
        print("performAnswerCallAction fail")
    }
    
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        print("performStartCallAction uuid = \(action.callUUID)")
        
        let sessionid = callManager.makeCallWithUUID(callee: action.handle.value, displayName: action.handle.value, videoCall: action.isVideo, uuid: action.callUUID)
        if sessionid >= 0 {
            action.fulfill()
        } else {
            action.fail()
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        let result = callManager.findCallByUUID(uuid: action.callUUID)
        if result != nil {
            callManager.hungUpCall(uuid: action.callUUID)
        }
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        let result = callManager.findCallByUUID(uuid: action.callUUID)
        if result != nil {
            callManager.holdCall(uuid: action.callUUID, onHold: action.isOnHold)
        }
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        let result = callManager.findCallByUUID(uuid: action.callUUID)
        if result != nil {
            callManager.muteCall(action.isMuted, uuid: action.callUUID)
        }
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        callManager.startAudio()
    }

    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        callManager.stopAudio()
    }
}
